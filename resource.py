import pygame, os

__images = {}
__fonts = {}
__sounds = {}

__images_path = None
__fonts_path = None
__sounds_path = None

def get_image(filename, force_reload = 0):
    
    if not __images_path:
        raise ValueError, "resources.set_images_path() not called yet."
    if (force_reload == 1 or filename not in __images.keys()):
        try:
            surface = pygame.image.load(os.path.join(__images_path, filename))
        except pygame.error:
            raise IOError, "File " + filename + " not found."
        __images[filename] = surface
        return surface
    else:
        return __images[filename]

def has_image(filename):
    return __images.has_key(filename)

def clear_image(filename):
    
    try:
        del __images[filename]
        return 1
    except KeyError:
        return 0


def get_font(filename, size, force_reload = 0):
    if not __fonts_path:
        raise ValueError, "resources.set_fonts_path() not called yet."
    if (force_reload == 1 or filename not in __fonts.keys()):
        try:
            font = pygame.font.Font(os.path.join(__fonts_path, filename), size)
        except pygame.error:
            raise IOError, "File " + filename + " not found."
        __fonts[filename] = font
        return font
    else:
        return __fonts[filename]

def has_font(filename):
    return __fonts.has_key(filename)

def clear_font(filename):
    try:
        del __fonts[filename]
        return 1
    except KeyError:
        return 0

def get_sound(filename, force_reload = 0):
    if not __sounds_path:
        raise ValueError, "resources.set_sounds_path() not called yet."
    if (force_reload == 1 or filename not in __fonts.keys()):
        try:
            sound = pygame.mixer.Sound(os.path.join(__sounds_path, filename))
        except pygame.error:
            raise IOError, "File " + filename + " not found."
        __sounds[filename] = sound
        return sound
    else:
        return __sounds[filename]

def has_sound(filename):
    return __sounds.has_key(filename)

def clear_sound(filename):
    try:
        del __sounds[filename]
        return 1
    except KeyError:
        return 0

def set_images_path(path):
    if not os.access(path, os.F_OK):
        raise IOError, path + " not found."
    if path.endswith(os.sep):
        path = path[:-1]
    global __images_path
    __images_path = path
    return 1

def set_fonts_path(path):
    if not os.access(path, os.F_OK):
        raise IOError, path + " not found."
    if path.endswith(os.sep):
        path = path[:-1]
    global __fonts_path
    __fonts_path = path
    return 1

def set_sounds_path(path):
    if not os.access(path, os.F_OK):
        raise IOError, path + " not found."
    if path.endswith(os.sep):
        path = path[:-1]
    global __sounds_path
    __sounds_path = path
    return 1

    
def get_images_path():
    return __images_path

def get_fonts_path():
    return __fonts_path

def get_sounds_path():
    return __sounds_path
