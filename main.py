import pygame
from pygame import *
import levels
import random
import resource
import os.path
import shelve

if os.path.exists("data/save"):
    ulozeni = shelve.open('data/save')
    bodiky2 = ulozeni['score']
    velikost = ulozeni['velikost']
    WIN_WIDTH = ulozeni['WIN_WIDTH']
    WIN_HEIGHT = ulozeni['WIN_HEIGHT']
    ulozeni.close()
else:
    bodiky2 = 0
    ulozeni = shelve.open('data/save')
    ulozeni['score'] = bodiky2
    velikost = 64
    WIN_WIDTH = 1024
    WIN_HEIGHT = 600
    ulozeni['velikost'] = velikost
    ulozeni['WIN_WIDTH'] = WIN_WIDTH
    ulozeni['WIN_HEIGHT'] = WIN_HEIGHT
    ulozeni.close()
HALF_WIDTH = int(WIN_WIDTH / 2)
HALF_HEIGHT = int(WIN_HEIGHT / 2)
prohra = False
vyhra = False
DISPLAY = (WIN_WIDTH, WIN_HEIGHT)
DEPTH = 0
FLAGS = 0
CAMERA_SLACK = 30
cerna = (0, 0, 0)
bila = (255, 255, 255)
cervena = (255, 0, 0)
zelena = (0, 255, 0)
modra = (0, 0, 255)
n = random.randint(0, 1)
v = 0
bodiky = 10
levels = {0: {'level': levels.level0()},
            1: {'level': levels.level1()},
            2: {'level': levels.level2()},
            3: {'level': levels.level3()},
            4: {'level': levels.level4()},
            5: {'level': levels.level5()},
            6: {'level': levels.level6()},
            7: {'level': levels.level7()},
            8: {'level': levels.level8()},
            9: {'level': levels.level9()},
            10: {'level': levels.level10()},
            11: {'level': levels.level11()},
            12: {'level': levels.level12()},
            13: {'level': levels.level13()},
            14: {'level': levels.level14()},
            15: {'level': levels.level15()},
            16: {'level': levels.level16()},
            17: {'level': levels.level17()},
            18: {'level': levels.level18()},
            19: {'level': levels.level19()},
            20: {'level': levels.level20()},
            21: {'level': levels.level21()},
            22: {'level': levels.level22()},
            23: {'level': levels.level23()},
            24: {'level': levels.level24()},
            25: {'level': levels.level25()}}


if velikost == 32:
    resource.set_images_path("data/32")
if velikost == 64:
    resource.set_images_path("data/64")


class Scene(object):
    def __init__(self):
        pass

    def render(self, screen):
        raise NotImplementedError

    def update(self):
        raise NotImplementedError

    def handle_events(self, events):
        raise NotImplementedError

class GameScene(Scene):
    def __init__(self, levelno):
        global v
        super(GameScene, self).__init__()
        self.bg = resource.get_image("bg2.gif", force_reload=0)
        self.entities = pygame.sprite.Group()
        self.platforms = []
        self.font = pygame.font.SysFont('Arial', 32)
        pygame.mouse.set_visible(False)
        if v == 0:
            self.score = str(0)
        else:
            self.score = str(bodiky2)
        self.text5 = self.font.render('Score: ' + self.score, True, (0, 0, 255))

        self.levelno = levelno
        v = levelno
        self.v = str(v)
        self.text6 = self.font.render('Level: ' + self.v + "/25", True, (0, 0, 255))

        levelinfo = levels[levelno]

        level = levelinfo['level']
        total_level_width = len(level[0]) * velikost
        total_level_height = len(level) * velikost

        # build the level
        x = 0
        y = 0
        self.g1 = False
        self.f1 = False
        for row in level:
            for col in row:
                if col == "P":
                    p = Platform(x, y)
                    self.platforms.append(p)
                    self.entities.add(p)
                if col == "E":
                    e = ExitBlock(x, y)
                    self.platforms.append(e)
                    self.entities.add(e)
                if col == "M":
                    m = Lava(x, y)
                    self.platforms.append(m)
                    self.entities.add(m)
                if col == "Z":
                    z = Lift(x, y)
                    self.platforms.append(z)
                    self.entities.add(z)
                if col == "A":
                    self.a = enemy(x, y)
                    self.platforms.append(self.a)
                    self.entities.add(self.a)
                if col == "H":
                    self.h = Player(x, y)
                    self.platforms.append(self.h)
                    self.entities.add(self.h)
                if col == "G":
                    self.g = Move(x, y)
                    self.platforms.append(self.g)
                    self.entities.add(self.g)
                    self.g1 = True
                if col == "F":
                    self.f = Move2(x, y)
                    self.platforms.append(self.f)
                    self.entities.add(self.f)
                    self.f1 = True
                x += velikost
            y += velikost
            x = 0

        self.camera = Camera(complex_camera, total_level_width, total_level_height)
        self.h.scene = self

    def render(self, screen):
        screen.blit(self.bg, (0, 0))

        for e in self.entities:
            screen.blit(e.image, self.camera.apply(e))
        screen.blit(self.text5, (0.5 * velikost, 32))
        screen.blit(self.text6, (0.5 * velikost, 64))

    def update(self):
        pressed = pygame.key.get_pressed()
        up, left, right = [pressed[key] for key in (K_SPACE, K_LEFT, K_RIGHT)]
        self.h.update(up, left, right, self.platforms)
        self.a.update(self.platforms)
        self.camera.update(self.h)
        if self.g1:
            self.g.update(self.platforms)
        else:
            pass
        if self.f1:
            self.f.update(self.platforms)
        else:
            pass

    def exit(self):
        self.manager.go_to(CustomScene("You win!"))
        global vyhra
        self.w = pygame.mixer.Sound("data/win.wav")
        pygame.mixer.Sound.play(self.w)
        vyhra = True

    def die(self):
        self.manager.go_to(CustomScene("You lose!"))
        global prohra
        self.l = pygame.mixer.Sound("data/lose.wav")
        pygame.mixer.Sound.play(self.l)
        prohra = True

    def handle_events(self, events):
        for e in events:
            if e.type == KEYDOWN and e.key == K_ESCAPE:
                self.manager.go_to(TitleScene())
            if e.type == KEYDOWN and e.key == K_F11:
                pygame.display.toggle_fullscreen()
            if e.type == KEYDOWN and e.key == K_m:
                pygame.mixer.music.pause()
            if e.type == KEYDOWN and e.key == K_n:
                pygame.mixer.music.unpause()


class CustomScene(object):

    def __init__(self, text):
        self.text = text
        super(CustomScene, self).__init__()
        self.font = pygame.font.SysFont('Arial', 56)

    def render(self, screen):
        text1 = self.font.render(self.text, True, cervena)
        screen.blit(text1, (240, 420))

    def update(self):
        pass

    def handle_events(self, events):
        global vyhra, prohra
        for e in events:
            if vyhra:
                if e.type == KEYDOWN and e.key == K_SPACE:
                    global v, bodiky, bodiky2
                    if v == 0:
                        bodiky1 = bodiky
                    else:
                        bodiky1 = bodiky2 + v
                    v += 1
                    if not bodiky1 == bodiky:
                        bodiky2 = bodiky + bodiky1
                    else:
                        bodiky2 = 0
                    if v == 26:
                        text20 = self.font.render("You win this game", True, cervena)
                        screen.blit(text20, (240, 350))
                    self.manager.go_to(GameScene(v))
                    ulozeni = shelve.open('data/save')
                    ulozeni['level'] = v
                    ulozeni['score'] = bodiky2
                    ulozeni.close()
                    vyhra = False
                    bodiky = 10
            elif prohra:
                if e.type == KEYDOWN and e.key == K_SPACE:
                    global v, bodiky
                    self.manager.go_to(GameScene(v))
                    prohra = False
                    if bodiky >= 1:
                        bodiky -= 1
            elif e.type == KEYDOWN:
                self.manager.go_to(TitleScene())


class TitleScene(object):

    def __init__(self):
        super(TitleScene, self).__init__()
        self.sfont = pygame.font.SysFont('Arial', velikost)
        pygame.mouse.set_visible(False)

    def render(self, screen):
        self.bg = resource.get_image("bg3.gif")
        text2 = self.sfont.render('> press SPACE to start <', True, (0, 255, 0))
        text3 = self.sfont.render('> press L to load game <', True, (255, 0, 0))
        text4 = self.sfont.render('> press H to see your score <', True, (0, 0, 255))
        text5 = self.sfont.render('> press O to go to options <', True, (0, 255, 255))
        screen.blit(self.bg, (0, 0))
        screen.blit(text2, (HALF_WIDTH / 3, 240))
        screen.blit(text3, (HALF_WIDTH / 3, 300))
        screen.blit(text4, (HALF_WIDTH / 3, 360))
        screen.blit(text5, (HALF_WIDTH / 3, 420))

    def update(self):
        pass

    def handle_events(self, events):
        for e in events:
            if e.type == KEYDOWN and e.key == K_SPACE:
                self.manager.go_to(GameScene(0))
            if e.type == KEYDOWN and e.key == K_F11:
                pygame.display.toggle_fullscreen()
            if e.type == KEYDOWN and e.key == K_l:
                if os.path.exists("data/save"):
                    global v
                    saveGameShelfFile = shelve.open('data/save')
                    v = saveGameShelfFile['level']
                    self.manager.go_to(GameScene(v))
                    saveGameShelfFile.close()
                else:
                    print("zadna data k nacteni")
            if e.type == KEYDOWN and e.key == K_h:
                if os.path.exists("data/save"):
                    saveGameShelfFile = shelve.open('data/save')
                    body = str(saveGameShelfFile['score'])
                    self.manager.go_to(CustomScene("Tvoje score je " + body))
                    saveGameShelfFile.close()
                else:
                    self.manager.go_to(CustomScene("Tvoje score je 0"))
            if e.type == KEYDOWN and e.key == K_o:
                self.manager.go_to(OptionsScene())


class OptionsScene(object):

    def __init__(self):
        super(OptionsScene, self).__init__()
        self.font = pygame.font.SysFont('Arial', velikost)
        pygame.mouse.set_visible(False)

    def render(self, screen):
        self.bg = resource.get_image("bg3.gif")
        text3 = self.font.render("Press Q for 640x480 resolution", True, modra)
        text4 = self.font.render("Press W for 800x600 resolution", True, modra)
        text5 = self.font.render("Press E for 1024x768 resolution", True, modra)
        text6 = self.font.render("Need restart to see changes", True, cerna)
        screen.blit(self.bg, (0, 0))
        screen.blit(text3, (HALF_WIDTH / 3.5, 280))
        screen.blit(text4, (HALF_WIDTH / 3.5, 340))
        screen.blit(text5, (HALF_WIDTH / 3.5, 400))
        screen.blit(text6, (HALF_WIDTH / 3.5, 460))


    def update(self):
        pass

    def handle_events(self, events):
        for e in events:
            if e.type == KEYDOWN and e.key == K_ESCAPE:
                self.manager.go_to(TitleScene())
            if e.type == KEYDOWN and e.key == K_q:
                ulozeni = shelve.open('data/save')
                WIN_WIDTH = 640
                WIN_HEIGHT = 480
                velikost = 32
                ulozeni['WIN_WIDTH'] = WIN_WIDTH
                ulozeni['WIN_HEIGHT'] = WIN_HEIGHT
                ulozeni['velikost'] = velikost
                ulozeni.close()
                self.manager.go_to(CustomScene("Finished"))
            if e.type == KEYDOWN and e.key == K_w:
                ulozeni = shelve.open('data/save')
                WIN_WIDTH = 800
                WIN_HEIGHT = 600
                velikost = 32
                ulozeni['WIN_WIDTH'] = WIN_WIDTH
                ulozeni['WIN_HEIGHT'] = WIN_HEIGHT
                ulozeni['velikost'] = velikost
                ulozeni.close()
                self.manager.go_to(CustomScene("Finished"))
            if e.type == KEYDOWN and e.key == K_e:
                ulozeni = shelve.open('data/save')
                WIN_WIDTH = 1024
                WIN_HEIGHT = 768
                velikost = 64
                ulozeni['WIN_WIDTH'] = WIN_WIDTH
                ulozeni['WIN_HEIGHT'] = WIN_HEIGHT
                ulozeni['velikost'] = velikost
                ulozeni.close()
                self.manager.go_to(CustomScene("Finished"))


class SceneMananger(object):
    def __init__(self):
        self.go_to(TitleScene())

    def go_to(self, scene):
        self.scene = scene
        self.scene.manager = self


def main():
    pygame.init()
    pygame.mixer.music.load("data/1.mp3")
    pygame.mixer.music.play(-1)
    screen = pygame.display.set_mode(DISPLAY, FLAGS, DEPTH)
    pygame.display.set_caption("PyGame Plosinovka")
    timer = pygame.time.Clock()
    running = True

    manager = SceneMananger()

    while running:
        timer.tick(60)

        if pygame.event.get(QUIT):
            running = False
            return
        manager.scene.handle_events(pygame.event.get())
        manager.scene.update()
        manager.scene.render(screen)
        pygame.display.flip()


class Camera(object):
    def __init__(self, camera_func, width, height):
        self.camera_func = camera_func
        self.state = Rect(0, 0, width, height)

    def apply(self, target):
        try:
            return target.rect.move(self.state.topleft)
        except AttributeError:
             return list(map(sum, list(zip(target, self.state.topleft))))

    def update(self, target):
        self.state = self.camera_func(self.state, target.rect)


def complex_camera(camera, target_rect):
    l, t, _, _ = target_rect
    _, _, w, h = camera
    l, t, _, _ = -l + HALF_WIDTH, -t + HALF_HEIGHT, w, h

    l = min(0, l)
    l = max(-(camera.width - WIN_WIDTH), l)
    t = max(-(camera.height - WIN_HEIGHT), t)

    return Rect(l, t, w, h)


class Entity(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)


class Player(Entity):
    def __init__(self, x, y):
        Entity.__init__(self)
        self.xvel = 0
        self.yvel = 0
        self.onGround = False
        self.hrac_r = resource.get_image('hrac.png')
        self.hrac_l = pygame.transform.flip(self.hrac_r, True, False)
        self.image = self.hrac_r
        #self.image = Surface((velikost, velikost))
        #self.image.convert()
        #self.image.fill(Color("#FF0000"))
        self.rect = Rect(x, y, velikost, velikost)

    def update(self, up, left, right, platforms):
        if up:
            if self.onGround:
                self.yvel -= velikost / 4
                self.j = pygame.mixer.Sound("data/jump.wav")
                pygame.mixer.Sound.play(self.j)
        if left:
            self.xvel = -velikost / 8
            self.image = self.hrac_l
        if right:
            self.xvel = velikost / 8
            self.image = self.hrac_r
        if not self.onGround:
            self.yvel += velikost * 0.0094
            if self.yvel > 100:
                self.yvel = 100
        if not(left or right):
            self.xvel = 0
        self.rect.left += self.xvel
        self.collide(self.xvel, 0, platforms)
        self.rect.top += self.yvel
        self.onGround = False
        self.collide(0, self.yvel, platforms)

    def collide(self, xvel, yvel, platforms):
        for p in platforms:
            if pygame.sprite.collide_rect(self, p):
                if isinstance(p, ExitBlock):
                    self.scene.exit()
                if isinstance(p, Platform):
                    if xvel > 0:
                        self.rect.right = p.rect.left
                    if xvel < 0:
                        self.rect.left = p.rect.right
                    if yvel > 0:
                        self.rect.bottom = p.rect.top
                        self.onGround = True
                        self.yvel = 0
                    if yvel < 0:
                        self.rect.top = p.rect.bottom
                if isinstance(p, Move):
                    if xvel > 0:
                        self.rect.right = p.rect.left
                    if xvel < 0:
                        self.rect.left = p.rect.right
                    if yvel > 0:
                        self.rect.bottom = p.rect.top
                        self.onGround = True
                        self.yvel = 0
                    if yvel < 0:
                        self.rect.top = p.rect.bottom
                if isinstance(p, Move2):
                    if xvel > 0:
                        self.rect.right = p.rect.left
                    if xvel < 0:
                        self.rect.left = p.rect.right
                    if yvel > 0:
                        self.rect.bottom = p.rect.top
                        self.onGround = True
                        self.yvel = 0
                    if yvel < 0:
                        self.rect.top = p.rect.bottom
                if isinstance(p, enemy):
                    self.scene.die()
                if isinstance(p, Lava):
                    self.scene.die()
                if isinstance(p, Lift):
                    self.yvel = -velikost / 4


class Platform(Entity):
    def __init__(self, x, y):
        Entity.__init__(self)
        #self.image = Surface((velikost, velikost))
        #self.image.convert()
        #self.image.fill(Color("#DDDDDD"))
        self.image = resource.get_image('ground.png')
        self.rect = Rect(x, y, velikost, velikost)

    def update(self):
        pass


class ExitBlock(Platform):
    def __init__(self, x, y):
        Platform.__init__(self, x, y)
        self.image = resource.get_image("exit.gif")
        #self.image.fill(Color("#0033FF"))


class Lava(Platform):
    def __init__(self, x, y):
        Platform.__init__(self, x, y)
        #self.image.fill(Color("#0055FF"))
        self.image = resource.get_image('lava.png')


class Lift(Entity):
    def __init__(self, x, y):
        Entity.__init__(self)
        self.image = resource.get_image("lift.png")
        #self.image = Surface((velikost, velikost))
        #self.image.convert()
        #self.image.fill(Color("#FF9900"))
        self.rect = Rect(x, y, velikost, velikost)


class Move(Entity):
    def __init__(self, x, y):
        Entity.__init__(self)
        #self.image = Surface((velikost, velikost))
        self.image = resource.get_image('grass.png')
        self.xvel = 0
        self.yvel = 0
        #self.image.convert()
        #self.image.fill(Color("#FF9900"))
        self.rect = Rect(x, y, velikost, velikost)
        self.n = 1

    def update(self, platforms):
        if self.n == 0:
            self.xvel = + velikost / 16
        if self.n == 1:
            self.xvel = - velikost / 16
        self.rect.left += self.xvel
        self.collide(self.xvel, 0, platforms)

    def collide(self, xvel, yvel, platforms):
        for p in platforms:
            if pygame.sprite.collide_rect(self, p):
                if isinstance(p, Platform):
                    global n
                    if xvel > 0:
                        self.rect.right = p.rect.left
                        self.n = 1
                    if xvel < 0:
                        self.rect.left = p.rect.right
                        self.n = 0
                if isinstance(p, Move2):
                    global n
                    if xvel > 0:
                        self.rect.right = p.rect.left
                        self.n = 1
                    if xvel < 0:
                        self.rect.left = p.rect.right
                        self.n = 0


class Move2(Entity):
    def __init__(self, x, y):
        Entity.__init__(self)
        #self.image = Surface((velikost, velikost))
        self.image = resource.get_image('dirt.png')
        self.xvel = 0
        self.yvel = 0
        #self.image.convert()
        #self.image.fill(Color("#FF9900"))
        self.rect = Rect(x, y, velikost, velikost)
        self.n = 1

    def update(self, platforms):
        if self.n == 0:
            self.xvel = + velikost / 16
        if self.n == 1:
            self.xvel = - velikost / 16
        self.rect.left += self.xvel
        self.collide(self.xvel, 0, platforms)

    def collide(self, xvel, yvel, platforms):
        for p in platforms:
            if pygame.sprite.collide_rect(self, p):
                if isinstance(p, Platform):
                    global n
                    if xvel > 0:
                        self.rect.right = p.rect.left
                        self.n = 1
                    if xvel < 0:
                        self.rect.left = p.rect.right
                        self.n = 0
                if isinstance(p, Move):
                    global n
                    if xvel > 0:
                        self.rect.right = p.rect.left
                        self.n = 1
                    if xvel < 0:
                        self.rect.left = p.rect.right
                        self.n = 0


class enemy(Entity):
    def __init__(self, x, y):
        Entity.__init__(self)
        #self.image = Surface((velikost, velikost))
        self.enemy_r = resource.get_image('enemy.png')
        self.enemy_l = pygame.transform.flip(self.enemy_r, True, False)
        self.image = self.enemy_r
        self.xvel = 0
        self.yvel = 0
        #self.image.convert()
        self.onGround = False
        #self.image.fill(Color("#00FF00"))
        self.rect = Rect(x, y, velikost, velikost)

    def update(self, platforms):
        if n == 0:
            self.xvel = + velikost / 8
            self.image = self.enemy_r
        if n == 1:
            self.xvel = - velikost / 8
            self.image = self.enemy_l
        if not self.onGround:
            self.yvel += velikost * 0.0094
            if self.yvel > 100:
                self.yvel = 100
        self.rect.left += self.xvel
        self.collide(self.xvel, 0, platforms)
        self.rect.top += self.yvel
        self.onGround = False
        self.collide(0, self.yvel, platforms)

    def collide(self, xvel, yvel, platforms):
        for p in platforms:
            if pygame.sprite.collide_rect(self, p):
                if isinstance(p, Platform):
                    global n
                    if xvel > 0:
                        self.rect.right = p.rect.left
                        n = 1
                    if xvel < 0:
                        self.rect.left = p.rect.right
                        n = 0
                    if yvel > 0:
                        self.rect.bottom = p.rect.top
                        self.onGround = True
                        self.yvel = 0
                    if yvel < 0:
                        self.rect.top = p.rect.bottom
                if isinstance(p, Lift):
                    self.yvel = -velikost/4


if __name__ == "__main__":
    main()
